app.controller("ProjectsController", function($http, $scope, $log, portfolioService){
    var portfolio = this;
    portfolio.searchFilterValue = "";
    portfolio.getProjects = getProjects;
    portfolio.setSearchFilter = setSearchFilter;
    function getProjects(){
        portfolioService.getProjects().then(function(response){
            portfolio.projects = response;
            console.log(response);
        });
    }
    
    function setSearchFilter(){
        portfolio.searchFilter = portfolio.searchFilterValue;
    }
    
    portfolio.getProjects();
});