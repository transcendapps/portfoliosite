app.factory('imageService', function($http, $q, $log){
    return {
        getImageString: getImageString,
    };
    
//    function getImageString(blob){
//        var deferred = $q.defer();
//        var reader = new FileReader();
//        reader.onload = function(event){
//            deferred.resolve(event.target.result); //event.target.results contains the base64 code to create the image.
//        };
//        reader.readAsDataURL(blob);//Convert the blob from clipboard to base64
//        return deferred;
//    }
    
    function getImageString(arrayBuffer) {
        var binary = '';
        var bytes = new Uint8Array( arrayBuffer );
        var len = bytes.byteLength;
        for (var i = 0; i < len; i++) {
            binary += String.fromCharCode( bytes[ i ] );
        }
        return 'data:image/png;base64,' + window.btoa( binary );
    }
    
});