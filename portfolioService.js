app.factory('portfolioService', function($http, $q, $log){
    return {
        getMembers: getMembers,
        getProjects: getProjects
    };
    
    function getMembers(){
        return $http.get("http://transcendapps.mynetgear.com:64001/portfolio").then(function(response){
            return response.data;         
        });
    }
    
    function getProjects(){
        return $http.get("http://transcendapps.mynetgear.com:64001/board/finished").then(function(response){
            return response.data;         
        });
    }
    
});