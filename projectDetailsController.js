(function(){
    'use strict';
    app.directive('projectDetails', projectDetails).controller('ProjectDetailsController', projectDetailsController);
    
    /**
     * @ngInject
     */
    
    function projectDetails(){
        return {
            restrict: 'E',
            scope: {
                project: '<'
            },
            templateUrl: 'projectDetails.html',
            controller: 'ProjectDetailsController',
            controllerAs: '$ctrl',
            replace: true,
            bindToController: true
        };
    }
    
    
    /**
     * @ngInject
     */
     
    function projectDetailsController($scope,$log, $http, imageService){
        var portfolio = this;
        portfolio.image = imageService.getImageString(portfolio.project.screenshot.data);
    }
})();