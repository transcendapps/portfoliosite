app.controller("TeamController", function($http, $scope, $log, portfolioService){
    var portfolio = this;
    portfolio.getMembers = getMembers;

        function getMembers(){
            //get the data from data.json using http and set the entries to the data
            portfolioService.getMembers().then(function(response){
                portfolio.members = response;
            });
        }
    
    portfolio.getMembers();
});