app.filter('projectNameFilter',function(){
    return function(entries, filterValue){
        if(!filterValue)
            return entries;
        var filterValueString = ""+filterValue;
        var newEntries = [];
        entries.forEach(function(item) {
            if(item.name.indexOf(filterValueString)>=0)
                newEntries.push(item);
        });
        return newEntries;
    };
});