(function(){
    'use strict';
    app.directive('member', member).controller('MemberController', memberController);
    
    /**
     * @ngInject
     */
    
    function member(){
        return {
            restrict: 'E',
            scope: {
                firstName : '<',
                lastName : '<',
                bio : '<',
                basedIn : '<',
                linkedin : '<',
                image : '<'
            },
            templateUrl: 'member.html',
            controller: 'MemberController',
            controllerAs: 'member',
            replace: true,
            bindToController: true
        };
    }
    
    
    /**
     * @ngInject
     */
     
    function memberController($scope,$log, $http, imageService){
        var portfolio = this;
       
        
        portfolio.image = imageService.getImageString(portfolio.image.data);
    }
})();